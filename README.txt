Google Summer of Code 2017 | Drupal

Project: Porting Uc wish list to Drupal 8

Student: Chiranjeeb Mahanta (musk_1107)

Mentor: Naveen Valecha (naveenvalecha)

Project Overview:

The UC wish list module, adds wish list/gift registry support to the Ubercart store, an open source e-commerce solution fully integrated with the leading open source CMS, Drupal. This module, for instance, would specifically allow customers to create and administer personalized wish lists of products in their Ubercart store. Other potential customers could then refer to those wish lists to get a better understanding about what items they should be purchasing and thereby purchase items on behalf of the wish list creators.
